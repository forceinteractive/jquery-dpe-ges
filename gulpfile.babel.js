import gulp         from 'gulp';
import browserify   from 'browserify';
import babelify     from 'babelify';
import source       from 'vinyl-source-stream';
import buffer       from 'vinyl-buffer';
import sourcemaps   from 'gulp-sourcemaps';
import watch        from 'gulp-watch';
import uglify       from 'gulp-uglify';
import path         from 'path';
import autoprefixer from 'gulp-autoprefixer';
import cssmin       from 'gulp-cssmin';
import rename       from 'gulp-rename';

const PATHS = {
    entryPoint : path.resolve(__dirname, 'src/index.js'),
    buildDirectory: path.resolve(__dirname, 'dist'),
    buildFilename: 'jquery-dpe-ges-min.js',
    watchJsFiles: [path.resolve(__dirname, 'src/*.js')],
    watchCssFiles: path.resolve(__dirname, 'css/*.css'),
    cssFile: path.resolve(__dirname, 'css/style.css')
};

gulp.task('browserify', bundler);

gulp.task('watch', () => {
    return watch(PATHS.watchJsFiles.concat(PATHS.watchCssFiles), (vinyl) => {
        if(vinyl.path.endsWith('.css')) {
            gulp.start('css');
        } else if(vinyl.path.endsWith('.js')) {
            gulp.start('browserify');
        }
    });
});

gulp.task('default', ['browserify', 'watch']);

gulp.task('css', () => {
    gulp.src(PATHS.cssFile)
        .pipe(autoprefixer({
            browsers: [
                "Android 2.3",
                "Android >= 4",
                "Chrome >= 20",
                "Firefox >= 24",
                "Explorer >= 8",
                "iOS >= 6",
                "Opera >= 12",
                "Safari >= 6"
            ],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min', basename: 'jquery-dpe-ges'}))
        .pipe(gulp.dest(PATHS.buildDirectory));
});

function bundler() {
    console.log('-> bundling ...');
    browserify({ debug: true })
        .transform(babelify)
        .require(PATHS.entryPoint, { entry: true })
        .bundle()
        .on("error", function (err) { console.log("Error: " + err.message); })
        .pipe(source(PATHS.buildFilename))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(PATHS.buildDirectory));
}