import pluginify from './pluginify';
const $ = window.jQuery || jQuery;

const TYPE_DPE = 'DPE';
const TYPE_GES = 'GES';
const DPE_COLORS = ['#59A402', '#68AB00', '#A1C300', '#EAE400', '#EEA500', '#D33E0E', '#CE1018'];
const GES_COLORS = ['#F5ECF4', '#D7BEDD', '#C7A8CE', '#B894C3', '#9D73AF', '#81549E', '#6C3E93'];
const DPE_VALUES = [50, 90, 150, 230, 330, 450, 450];
const GES_VALUES = [5, 10, 20, 35, 55, 80, 80];

const ALPHABET = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

class Plugin {
    constructor( options ) {

        this.checkArguments(options);

        this.colors = options.type === TYPE_DPE ? DPE_COLORS : GES_COLORS;
        this.values = options.type === TYPE_DPE ? DPE_VALUES : GES_VALUES;
        this.suffix = '=< ';
        this.prefix = '> ';
        this.separator = ' à ';
        this.startWidth = 12;

        Object.assign(this, options);

        this.init();
    }

    checkArguments(options) {
        // Check parameters
        if(options.type !== TYPE_DPE && options.type !== TYPE_GES) {
            throw new Error('Type must be in (' + TYPE_DPE + ', ' + TYPE_GES + ')' + options.type + ' was given instead');
        }
    }

    init() {

        let idx = this.getIndicatorIndex();

        let outputHtml = '<div class="dpe-ges">';
            outputHtml +=   this.constructHeader();
            outputHtml +=   this.constructRules(idx);
            outputHtml += '</div>';

        $(this.el).html(outputHtml);

    }

    /**
     * Méthode permmettant de savoir à quel index la flêche indiquant la valeur doit être placé
     * @returns {number}
     */
    getIndicatorIndex() {
        const dpeLen = DPE_VALUES.length - 1;

        return this.values.findIndex((value, index) => {

            if(this.value < value) {
                return true;
            }

            if(index < dpeLen && this.value <= value) {
                return true;
            }

            if(index === dpeLen && this.value > value) {
                return true;
            }

        });
    }

    /**
     * Construction du Header du graphique
     * @returns {string}
     */
    constructHeader() {
        return `<div class="header">
                    <span class="heading">${this.headingLeft}</span>
                    <span class="heading">${this.headingRight}</span>
                </div>`;
    }

    /**
     * Construction des règles
     * @param idx {Number} Indique l'endroit où la valeur entrée par l'utilisateur doit être affichée
     * @returns {string}
     */
    constructRules(idx) {
        let width = this.startWidth;
        let rulesHtml = '';

        this.colors.forEach((colorHexa, index) => {

            rulesHtml += `<div class="arrow-container">
                        <div class="arrow-box" data-color="${colorHexa}" style="background-color: ${colorHexa}; width: ${width}%">
                            <span class="value">${this.formatValue(index)}</span>
                            <span class="letter">${ALPHABET[index]}</span>
                            <div class="arrow-right" style="border-left-color: ${colorHexa}"></div>
                        </div>`;

            if(idx === index) {
                rulesHtml += `<div class="number-container">
                            <div class="number">${this.value} <div class="arrow-left"></div></div>
                            <span class="lower">KW/h<sub>EP</sub>m<sup>3</sup>an</span>
                        </div>`;
            }

            rulesHtml += '</div>';

            width += 10; // Percent

        });

        return rulesHtml;
    }

    /**
     * Format de la valeur à afficher concernant les paliers
     * @param index {Number} Index du tableau parcouru
     * @returns {string}
     */
    formatValue(index) {

        let returnStr = '';

        if(index === 0) {
            returnStr += this.suffix + DPE_VALUES[index];
        }

        if(index > 0 && index < DPE_VALUES.length - 1) {
            returnStr += (DPE_VALUES[index - 1] + 1) + this.separator + DPE_VALUES[index];
        }

        if(index === DPE_VALUES.length - 1) {
            returnStr += this.prefix + DPE_VALUES[index];
        }

        return returnStr;
    }
}

/*
 Convert plugin class into a jQuery plugin
 */
pluginify('dpeToImage', 'dpeToImage', Plugin, true);

export default Plugin;