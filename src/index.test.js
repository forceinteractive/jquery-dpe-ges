import plugin from './index';

test('It should throw an error if no type is passed', () => {
    expect(() => {
        new plugin();
    }).toThrow();
});

test('It should not throw an error if type is passed', () => {
    expect(() => {
        new plugin({
            type: 'DPE'
        });
    }).not.toThrow();
});

test('Index should be 6', () => {
    const p = new plugin({type: 'DPE', value: 451});
    expect(p.getIndicatorIndex()).toBe(6);
});

test('Index should be 5', () => {
    const p = new plugin({type: 'DPE', value: 450});
    expect(p.getIndicatorIndex()).toBe(5);
});

test('Index should be 0', () => {
    const p = new plugin({type: 'DPE', value: 48});
    expect(p.getIndicatorIndex()).toBe(0);
});

test('It should format suffix properly', () => {
    const p = new plugin({type: 'DPE', value: 48, suffix: '=< '});
    expect(p.formatValue(0)).toMatch("=< 50");
});

test('It should format suffix properly', () => {
    const p = new plugin({type: 'DPE', value: 48, prefix: '> '});
    expect(p.formatValue(6)).toMatch("> 450");
});